﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.InputMethods;
using Android.Text;

namespace DroidUtil.Xamarin
{
    public static class InputUtils
    {

        // TODO LATER why not just use Window.SetSoftInputMode(SoftInput.StateHidden, etc) ?
        public static void ToggleSoftKeyboard(Activity activity, bool showKeyboard)
        {
            var focusedView = activity.CurrentFocus;
            if (focusedView == null)
                return;
            var inputManager = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
            if (showKeyboard)
                inputManager.ShowSoftInput(focusedView, ShowFlags.Implicit);
            else
                inputManager.HideSoftInputFromWindow(focusedView.WindowToken, HideSoftInputFlags.None);
        }

        /// <summary>
        /// Forces this <see cref="EditText"/> to use the provided <see cref="ImeAction"/>,
        /// showing the appropriate button in the soft keyboard.
        /// Normally, the XML-specified <see cref="ImeAction"/> is ignored if the EditText hasn't set
        /// <see cref="TextView.SetSingleLine()"/>, or has a fixed number of lines being set using <see cref="TextView.SetLines"/>.
        /// This made it impossible to create a field accepting a single line input, but displaying it on multiple lines
        /// with word wrapping (instead of horizontally scrolling), while showing the desired IME Action button instead
        /// of the default carriage return button.
        /// </summary>
        /// <param name="inputTypes">Additional flags, such as word / sentence captialization, etc.</param>
        public static void ForceImeAction(this EditText editText, ImeAction imeAction, InputTypes inputTypes = InputTypes.ClassText)
        {
            editText.ImeOptions = imeAction;
            editText.SetRawInputType(inputTypes);
        }

        /// <summary>
        /// Converts a HTML string to an <see cref="SpannedString"/> instance.
        /// which can be used with <see cref="TextView.TextFormatted"/>.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static SpannedString ParseHtml(string htmlString)
        {
            var spanned = Build.VERSION.SdkInt >= BuildVersionCodes.N
                ? Html.FromHtml(htmlString, FromHtmlOptions.ModeLegacy)
#pragma warning disable 618
                : Html.FromHtml(htmlString);
#pragma warning restore 618
            return new SpannedString(spanned);
        }

    }
}