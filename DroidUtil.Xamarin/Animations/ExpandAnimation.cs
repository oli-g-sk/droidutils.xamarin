﻿using Android.Views;
using Android.Util;

namespace DroidUtil.Xamarin.Animations
{

    public class ExpandAnimation : BaseAnimation
    {

        private const string Tag = "ExpandAnimation";

        int startingHeight;
        int targetHeight;
        int heightDifferenceToAnimate;

        protected override void InitAnimation()
        {

            base.InitAnimation();
            AttachedView.Visibility = ViewStates.Visible;

            // Older versions of android (pre API 21) cancel animations for views with a height of 0.
            // v.getLayoutParams().height = 1; TODO OK? this is disabled to allow animating from "current" height

            startingHeight = AttachedView.MeasuredHeight;
            Log.Debug(Tag, "expand. starting height: " + startingHeight + " px");
            if (targetHeight == ViewGroup.LayoutParams.WrapContent)
            {
                // the negative constant representing WRAP_CONTENT must be replaced by actual measured value
                AttachedView.Measure(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                targetHeight = AttachedView.MeasuredHeight;
                Log.Debug(Tag, "expand. target height: WRAP_CONTENT (" + targetHeight + " px)");
            }
            Log.Debug(Tag, "expand. target height: " + targetHeight + " px");
            heightDifferenceToAnimate = targetHeight - startingHeight;
            Log.Debug(Tag, "expand. height diff to animate: " + heightDifferenceToAnimate + " px");

        }

        /// <summary>
        /// Create an ExpandAnimation that will expand the view's height until it's WRAP_CONTENT
        /// </summary>
        public ExpandAnimation(View view) : this(view, -1) { }

        public ExpandAnimation(View view, int targetHeightPx) : base(view)
        {
            targetHeight = targetHeightPx;
        }

        protected override void OnAnimationStep(float interpolatedTime)
        {

            var transformedHeight = (int)(startingHeight + (heightDifferenceToAnimate * interpolatedTime));
            Log.Verbose(Tag, "transformed height: " + transformedHeight);
            AttachedView.LayoutParameters.Height = transformedHeight;
            /* interpolatedTime == 1
            ? ViewGroup.LayoutParams.WRAP_CONTENT : */
            AttachedView.RequestLayout();
        }

        public override bool WillChangeBounds()
        {
            return true;
        }

    }

}