﻿using System;
using Android.Views;
using Android.Views.Animations;
using Android.Util;

namespace DroidUtil.Xamarin.Animations
{

    /// <summary>
    /// A replacement for <see cref="Animation"/> solving some of its annoyances.
    /// See <see cref="OnAnimationStep(float)"/>
    /// </summary>
    public abstract class BaseAnimation : Animation
    {

        private const string Tag = "BaseAnimation";

        protected View AttachedView { get; private set; }
        protected bool WasInitialized { get; private set; }
        protected new bool HasEnded { get; private set; }

        private readonly string associatedViewName;

        protected BaseAnimation(View view)
        {
            AttachedView = view;
            WasInitialized = false;
            associatedViewName = AttachedView.ToString();
        }

        protected virtual void InitAnimation()
        {
            if (AttachedView == null)
                throw new InvalidOperationException("The animation can only run once, please use a new instance");
            WasInitialized = true;
        }

        protected sealed override void ApplyTransformation(float interpolatedTime, Transformation t)
        {

            // already finished
            if (HasEnded)
            {
                Log.Warn(Tag, $"running on {associatedViewName} after HasEnded set to true, with interpolatedTime: {interpolatedTime}");
                return;
            }

            // first frame, not yet initialized
            if (!WasInitialized)
                InitAnimation();

            OnAnimationStep(interpolatedTime);

            // last frame, ending
            if (interpolatedTime.Equals(1))
            {
                Log.Verbose(Tag, "last frame (interpolatedTime is 1)");
                OnAnimationEnd();
            }

        }

        /// <summary>
        /// Implement to apply the animated value to attached view.
        /// Intentionaly replaces the usual (now sealed) <see cref="Animation.ApplyTransformation(float, Transformation)"/>
        /// callback because of a bug(?) in the API.
        /// </summary>
        /// <remarks>
        /// The problem: https://stackoverflow.com/questions/4750939/android-animation-is-not-finished-in-onanimationend
        /// Basically AnimationEnd event is raised "almost at the end", but it's not 100%, and most of all,
        /// ApplyTransformation callback might FIRE AGAIN AFTER the end event. And since we're using the end event
        /// to clear the View reference, subsequent call to ApplyTransformation would cause NREs in any subclass
        /// trying to manipulate the View in ApplyTransformation override.
        /// That's why this class seals the callback, replaces it with this method, and stops calling it once
        /// an interpolated value of 1.00 "arrived" in ApplyTransformation, which is a sufficient indication of animation ending.
        /// override
        /// </remarks>
        /// <param name="interpolatedTime"></param>
        protected abstract void OnAnimationStep(float interpolatedTime); // TODO TEST

        // NOTE: DO NOT use the AnimationEnd event or AnimationListener interface, see
        // https://stackoverflow.com/questions/4750939/android-animation-is-not-finished-in-onanimationend
        // basically AnimationEnd event is unreliable, gets called towards the end of animation but ApplyTransformation still fires after it
        private void OnAnimationEnd()
        {
            Log.Debug(Tag, $"ending on view {AttachedView}; removing reference");
            AttachedView = null;
            HasEnded = true;
        }

    }

}