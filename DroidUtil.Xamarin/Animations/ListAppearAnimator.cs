﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android;
using static DroidUtil.Xamarin.AnimUtils;

namespace DroidUtil.Xamarin.Animations
{

    /// <summary>
    /// Allows to perform a simple appear animation on ViewGroup's children.
    /// Meant to be used on a ListView or a RecyclerView for animating their rows,
    /// but should work with any ViewGroup.
    /// </summary>
    /// <remarks>
    /// Originally this class only accepted ListView or RecyclerView instances for its public methods
    /// by design, but that created an unnecessary dependency on the RecyclerView support package.
    /// </remarks>
    public class ListViewFadeInAnimator : Java.Lang.Object, ViewTreeObserver.IOnPreDrawListener
    {

        private ViewGroup list;

        private readonly bool isHorizontal;
        private readonly int itemDurationMs;
        private readonly int itemDelayMs;
        private readonly int startDelayMs;
        private readonly int translationPx;

        private const int DefaultTranslationDp = 8;
        // TODO allow to specify translation

        private ListViewFadeInAnimator(ViewGroup list, bool isHorizontal, int itemDurationMs, int itemDelayMs, int startDelayMs)
        {
            this.list = list;
            this.isHorizontal = isHorizontal;
            this.itemDurationMs = itemDurationMs;
            this.itemDelayMs = itemDelayMs;
            this.startDelayMs = startDelayMs;
            this.translationPx = ResUtils.DpToPx(list.Context, DefaultTranslationDp);
        }

        public static void AnimateItemAppearance(ViewGroup list)
        {
            AnimateItemAppearance(list, false, Duration.Short);
        }

        public static void AnimateItemAppearance(ViewGroup list, bool isHorizontal, Duration itemDuration)
        {
            var itemAnimDuration = GetDuration(list.Context, itemDuration);
            var itemAnimDiff = itemAnimDuration / 2;
            list.ViewTreeObserver.AddOnPreDrawListener(
                new ListViewFadeInAnimator(list, isHorizontal, itemAnimDuration, itemAnimDiff, 0));
        }

        public static void AnimateItemAppearance(ViewGroup list, bool isHorizontal,
            int itemDurationMs, int itemDelayMs, int startDelayMs)
        {
            list.ViewTreeObserver.AddOnPreDrawListener(
                new ListViewFadeInAnimator(list, isHorizontal, itemDurationMs, itemDelayMs, startDelayMs));
        }

        public bool OnPreDraw()
        {

            if (list == null)
            {
                return false;
            }

            // schedule animation for each child
            for (int i = 0; i < list.ChildCount; i++)
            {
                var view = list.GetChildAt(i);
                view.Alpha = 0.0f;
                if (isHorizontal)
                {
                    view.TranslationX = translationPx;
                }
                else
                {
                    view.TranslationY = translationPx;
                }
                int startDelay = i * itemDelayMs;
                view.PostDelayed(() => {
                    if (isHorizontal)
                    {
                        view.Animate()
                            .Alpha(1.0f)
                            .TranslationX(0)
                            .SetDuration(itemDurationMs)
                            .SetStartDelay(startDelay);
                    }
                    else
                    {
                        view.Animate()
                            .Alpha(1.0f)
                            .TranslationY(0)
                            .SetDuration(itemDurationMs)
                            .SetStartDelay(startDelay);
                    }
                }, startDelayMs);
            }

            // remove references
            list.ViewTreeObserver.RemoveOnPreDrawListener(this);
            list = null;

            return true;

        }
    }

}