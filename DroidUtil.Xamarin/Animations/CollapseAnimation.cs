﻿using Android.Views;
using Android.Util;

namespace DroidUtil.Xamarin.Animations
{

    public class CollapseAnimation : BaseAnimation
    {

        // TODO test with custom target height

        private const string Tag = "CollapseAnimation";

        private int initialHeight;

        public CollapseAnimation(View view) : base(view) { }

        protected override void InitAnimation()
        {
            base.InitAnimation();
            initialHeight = AttachedView.MeasuredHeight;
            Log.Debug(Tag, "initAnimation, initial height: " + initialHeight);
        }

        protected override void OnAnimationStep(float interpolatedTime)
        {

            // MyLog.Trace(TAG, "applyTransformation, interpolatedTime: " + interpolatedTime);
            var calculatedHeight = initialHeight - (int)(initialHeight * interpolatedTime);
            Log.Verbose(Tag, "applyTransformation, height: " + calculatedHeight);
            if (calculatedHeight == 0)
            {
                AttachedView.Visibility = ViewStates.Gone;
            }
            else
            {
                AttachedView.LayoutParameters.Height = calculatedHeight;
                AttachedView.RequestLayout();
            }
        }

        public override bool WillChangeBounds()
        {
            return true;
        }

    }

}