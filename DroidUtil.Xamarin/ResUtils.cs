﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.Annotation;
using Android.Support.V4.Content;
using Android.Support.V4.Graphics.Drawable;
using Android.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DroidUtil.Xamarin
{
    public static class ResUtils
    {

        /// <summary>
        /// Convert a dimension resource to pixel value; basically just a convenience
        /// method for calling <see cref="Android.Content.Res.Resources.GetDimensionPixelSize(int)"/>
        /// </summary>
        /// <param name="dpResource">ID of a dimension resource</param>
        public static int DpToPx(Context context, [DimenRes]int dpResource)
        {
            return context.Resources.GetDimensionPixelSize(dpResource);
        }

        public static int DpToPx(Context context, float dpValue)
        {
            var pixels = TypedValue.ApplyDimension(ComplexUnitType.Dip, dpValue, context.Resources.DisplayMetrics);
            return (int) Math.Round(pixels);
        }

        public static float SpToPx(Context context, float sp)
        {
            var scale = context.Resources.DisplayMetrics.ScaledDensity;
            return sp * scale;
        }

        /// <summary>
        /// Loads a drawable resource, directly applying specified tint color.
        /// </summary>
        /// <remarks>
        /// https://android-developers.googleblog.com/2009/05/drawable-mutations.html
        /// </remarks>
        public static Drawable GetTintedDrawable(Context context, [DrawableRes]int drawable, [ColorRes]int tintColor)
        {
            if (context == null)
                return null;
            var loadedDrawable = ContextCompat.GetDrawable(context, drawable);
            var wrappedDrawable = DrawableCompat.Wrap(loadedDrawable.Mutate());
            var tintColorInt = ContextCompat.GetColor(context, tintColor);
            DrawableCompat.SetTintMode(wrappedDrawable, PorterDuff.Mode.SrcIn);
            DrawableCompat.SetTint(wrappedDrawable, tintColorInt);
            return wrappedDrawable;
        }

        /// <summary>
        /// Convenience method for mutating and tinting a <see cref="Drawable"/>
        /// using <see cref="Drawable.SetColorFilter(Color, PorterDuff.Mode)"/>
        /// </summary>
        /// <remarks>
        /// https://android-developers.googleblog.com/2009/05/drawable-mutations.html
        /// </remarks>
        public static void TintDrawable(this Drawable drawable, Context context, [ColorRes]int tintColor)
        {
            var color = new Color(ContextCompat.GetColor(context, tintColor));
            drawable.Mutate().SetColorFilter(color, PorterDuff.Mode.SrcIn);
        }

        public static Color BlendColors(Context context, [ColorRes]int colorFrom, [ColorRes]int colorTo, float ratio)
        {
            var from = ContextCompat.GetColor(context, colorFrom);
            var to = ContextCompat.GetColor(context, colorTo);
            return BlendColors(from, to, ratio);
        }

        public static Color BlendColors([ColorInt]int colorFrom, [ColorInt]int colorTo, float ratio)
        {
            float inverseRatio = 1f - ratio;
            float r = Color.GetRedComponent(colorTo) * ratio + Color.GetRedComponent(colorFrom) * inverseRatio;
            float g = Color.GetGreenComponent(colorTo) * ratio + Color.GetGreenComponent(colorFrom) * inverseRatio;
            float b = Color.GetBlueComponent(colorTo) * ratio + Color.GetBlueComponent(colorFrom) * inverseRatio;
            return Color.Rgb((int)r, (int)g, (int)b);
        }

    }
}
