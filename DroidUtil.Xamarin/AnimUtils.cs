﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using DroidUtil.Xamarin.Animations;

namespace DroidUtil.Xamarin
{
    public static class AnimUtils
    {

        public static int GetDuration(Context context, Duration duration)
        {
            int durationRes = -1;
            switch (duration) {
                case Duration.Short:
                    durationRes = Android.Resource.Integer.ConfigShortAnimTime;
                    break;
                case Duration.Medium:
                    durationRes = Android.Resource.Integer.ConfigMediumAnimTime;
                    break;
                case Duration.Long:
                    durationRes = Android.Resource.Integer.ConfigLongAnimTime;
                    break;
            }
            int durationMs = context.Resources.GetInteger(durationRes);
            return durationMs;
        }

        /// <summary>
        /// An awaitable convenience method to fade in a view.
        /// If the view is not visible, it is set to <see cref="ViewStates.Visible"/>
        /// with initial alpha of 0.
        /// </summary>
        /// <param name="view">The Viee to (show and) fade-in</param>
        /// <param name="duration">Duration; defaults to <see cref="Android.Resource.Integer.ConfigShortAnimTime"/></param>
        /// <param name="delay">Optional delay</param>
        /// <returns></returns>
        public static async Task FadeIn(this View view, Duration duration = Duration.Short, int delay = 0)
        {

            var durationMs = GetDuration(view.Context, duration);
            view.PostDelayed(() =>
            {
                if (view.Visibility != ViewStates.Visible)
                {
                    view.Visibility = ViewStates.Visible;
                    view.Alpha = 0F;
                }
                view.Animate().Alpha(1F);
            }, delay);

            if (delay > 0)
                await Task.Delay(delay);

        }

        /// <summary>
        /// An awaitable convenience method to fade out a view.
        /// If the view is not visible or its alpha is already 0, nothing happens.
        /// </summary>
        /// <param name="view">The Viee to (show and) fade-in</param>
        /// <param name="duration">Duration; defaults to <see cref="Android.Resource.Integer.ConfigShortAnimTime"/></param>
        /// <param name="delay">Optional delay</param>
        /// <returns></returns>
        public static async Task FadeOut(this View view, Duration duration = Duration.Short, int delay = 0)
        {

            var durationMs = GetDuration(view.Context, duration);
            view.PostDelayed(() =>
            {
                view.Animate().Alpha(0F);
            }, delay);

            if (delay > 0)
                await Task.Delay(delay);

        }

        public enum Duration
        {
            Short,
            Medium,
            Long
        }

    }
}